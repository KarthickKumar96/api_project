﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AddNumber.Controllers
{
    public class HomeController : ApiController
    {
        [Route("api/Home/{num1}/{num2}")]
        [HttpGet]
        //http://localhost:2583/api/Home?num1=2&num2=5
        public int Add(int num1, int num2)
        {
            int result = num1 + num2;
            return result;
        }

        public string Get()
        {
            return "Hello Welcome";
        }

    }
}
